<?php

/**
 * Project:     shootevents
 * File:        shootevents.module
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @file
 * @link http://drupal.org/
 * @copyright 2005-2006 TIRO Communication
 * @author Hinrich Donner <hd at tiro-communication dot net>
 * @package shootevents
 * @version 0.1.3
 */

/*
 * Some definitions. There is no need to change it here. Use the settings...
 */
define('SHOOTEVENTS_DIS_DEFAULT', 'Compak-Sporting,Doubles,Parcours de Chasse,ZZ');
define('SHOOTEVENTS_TARGETS_DEFAULT', '25,50,75,100,125,150,200,250,300');
define('SHOOTEVENTS_MAXDISP_BLOCK', 7);
define('SHOOTEVENTS_MAXDISP_LIST', 25);
define('SHOOTEVENTS_DATE_SHORT', '%d.%m.%y');
define('SHOOTEVENTS_DATE_LONG', '%e. %B %Y');

/**
 * Initialize Module
 *
 * @version 2
 */
function shootevents_init() {

  if (function_exists('drupal_get_path')) {

    $path   = drupal_get_path('module', 'shootevents');
    $css    = $path . '/shootevents.css';
    theme_add_style($css);
  }
}

/**
 * Cron
 *
 * Remove old events.
 *
 * @version 1
 */
function shootevents_cron()
{
    // Delete expired entries
    $sql = sprintf("DELETE FROM {shootevents} WHERE start<'%s'", strftime("%Y-%m-%d", time()));
    db_query(db_rewrite_sql($sql));
}

/**
 * Bind Menu Items
 * @return array
 * @version 1
 */
function shootevents_menu()
{
    $items = array();
    $items[] = array('path'         => 'shootevents',
                     'title'        => t("shoot events"),
                     'callback'     => '_shootevents_all',
                     'access'       => user_access('access shootevents'),
                     'type'         => MENU_NORMAL_ITEM);

    $items[] = array('path'         => 'shootevents/add',
                     'title'        => t("add a shoot events"),
                     'callback'     => '_shootevents_add',
                     'access'       => user_access('create shootevents'),
                     'type'         => MENU_NORMAL_ITEM);

    $items[] = array('path'         => 'shootevents/view',
                     'title'        => t("shoot events"),
                     'callback'     => '_shootevents_all',
                     'access'       => user_access('access shootevents'),
                     'type'         => MENU_CALLBACK);

    $items[] = array('path'         => 'shootevents/modify',
                     'title'        => t("edit a shoot events"),
                     'callback'     => '_shootevents_modify',
                     'access'       => user_access('create shootevents'),
                     'type'         => MENU_CALLBACK);

    $items[] = array('path'         => 'shootevents/delete',
                     'title'        => t("delete a shoot events"),
                     'callback'     => '_shootevents_delete',
                     'access'       => user_access('create shootevents'),
                     'type'         => MENU_CALLBACK);
    return $items;
}

/**
 * Display help and module information
 * @param section which section of the site we're displaying help
 * @return help text for section
 * @version 2
 */
function shootevents_help($section = '') {

  $output = '';
  switch ($section) {
    case "admin/modules#description":
      $output = t("Displays a list of shooting events");
      break;

    case "admin/help#shootevents":
      $output = t("This module manage a simple list of shooting events.");
      break;

    case 'admin/settings/shootevents':
      $output = t('Manage the disciplines, targets and list styles.')
        .' '.t("Remember to customize the style sheet stored in <em>shootevents.css</em> in your module directory.");
      break;
  }
  return $output;
}

/**
 * Set Up Access Levels
 * @version 1
 */
function shootevents_perm() {

  return array('access shootevents',
               'create shootevents',
               'administer shootevents');
}

/**
 * Generate HTML for the shootevents block
 * @param op the operation from the URL
 * @param delta offset
 * @returns block HTML
 * @version 3
 */
function shootevents_block($op = 'list', $delta = 0, $edit = array())
{
    // listing of blocks, such as on the admin/block page
  if ($op == "list") {
    $block[0]["info"] = t('Shooting events');
    return $block;
  }
  elseif ($op == 'configure' && $delta == 0) {
    $form['shootevents_maxdisp_block']      = array(
      '#type'             => 'textfield',
      '#title'            => t("Maximum number of events in the block"),
      '#description'      => t("The maximum number of links to display in the block."),
      '#default_value'    => variable_get("shootevents_maxdisp_block", SHOOTEVENTS_MAXDISP_BLOCK),
      '#size'             => 3,
      '#maxlength'        => 3,
      '#required'         => true
    );
    $form['shootevents_date_short']      = array(
      '#type'             => 'textfield',
      '#title'            => t("Date format"),
      '#description'      => t("The date format in the block.") . ' ' . t("Refer PHP manual for strftime() to learn more about the format."),
      '#default_value'    => variable_get("shootevents_date_short", SHOOTEVENTS_DATE_SHORT),
      '#size'             => 25,
      '#maxlength'        => 255,
      '#required'         => true
    );
    return $form;
  }
  elseif ($op == 'save' && $delta == 0) {
    variable_set('shootevents_maxdisp_block', $edit['shootevents_maxdisp_block']);
    variable_set('shootevents_date_short', $edit['shootevents_date_short']);
    return;
  }
  else {
    // Check access
    if (!user_access('access shootevents'))
        return array();

    // Load items
    //
    $lim = variable_get("shootevents_maxdisp_block", SHOOTEVENTS_MAXDISP_BLOCK);
    $sql = sprintf("SELECT e.name,
                           e.start,
                           e.dis,
                           e.targets
                    FROM {shootevents} e
                    WHERE e.start>='%s'
                    ORDER BY e.start ASC",
                   strftime('%Y-%m-%d', time()));
    $dbr = db_query_range(db_rewrite_sql($sql), 0, $lim);
    if (db_num_rows($dbr) == 0)
      return array();
    else {
      // Prepare output
      $_dark = false;
      $format = variable_get("shootevents_date_short", SHOOTEVENTS_DATE_SHORT);
      $content = "<table class=\"block-shootevents\">\n";

      while ($row = db_fetch_object($dbr)) {

        $dark = ($_dark ? 'dark' : 'light');
        $_dark = !$_dark;
        $content .= "<tr class=\"$dark\">"
            . "<td class=\"$dark\"><strong>"
            . strftime($format, _shootevents_convert_date($row->start, true))
            . ":</strong> " . check_plain($row->name) . "<br /><small>"
            . sprintf(t("%s targets %s"), check_plain($row->targets), check_plain($row->dis))
            . "</small></td>\n</tr>\n";
      }
      $content .= "</table>\n";
    }

    // our block content
    $block['subject'] = t("Upcoming Events");
    $block['content'] = $content
        . "<div class=\"more-link\">".
        l(t("more"), "shootevents", array("title" => t("More events...")))
        ."</div>";

    return $block;
  }
}

/**
 * Module configuration settings
 *
 * @return settings HTML or deny access
 * @version 2
 */
function shootevents_settings() {

  // only administrators can access this module
  if (!user_access('administer shootevents'))
      return message_access();

  $form['shootevents_general'] = array(
    '#type'             => 'fieldset',
    '#title'            => t('General Settings'),
    '#collapsible'      => true,
    '#collapsed'        => false
  );
  $form['shootevents_general']['shootevents_dis'] = array(
    '#type'             => 'textfield',
    '#title'            => t("Disciplines"),
    '#description'      => t("Define the possible disciplines. Each discipline has to be seperated by comma."),
    '#default_value'    => variable_get("shooterevents_dis", SHOOTEVENTS_DIS_DEFAULT),
    '#size'             => 60,
    '#maxlength'        => 65535,
    '#required'         => true
  );
  $form['shootevents_general']['shootevents_targets'] = array(
    '#type'             => 'textfield',
    '#title'            => t("Targets"),
    '#description'      => t("Define the possible number of targets. Each value has to be seperated by comma."),
    '#default_value'    => variable_get("shooterevents_targets", SHOOTEVENTS_TARGETS_DEFAULT),
    '#size'             => 60,
    '#maxlength'        => 65535,
    '#required'         => true
  );


  $form['shootevents_block'] = array(
    '#type'             => 'fieldset',
    '#title'            => t('Block Settings'),
    '#collapsible'      => true,
    '#collapsed'        => true
  );
  $form['shootevents_block']['shootevents_maxdisp_block']      = array(
    '#type'             => 'textfield',
    '#title'            => t("Maximum number of events in the block"),
    '#description'      => t("The maximum number of links to display in the block."),
    '#default_value'    => variable_get("shootevents_maxdisp_block", SHOOTEVENTS_MAXDISP_BLOCK),
    '#size'             => 3,
    '#maxlength'        => 3,
    '#required'         => true
  );
  $form['shootevents_block']['shootevents_date_short']      = array(
    '#type'             => 'textfield',
    '#title'            => t("Date format"),
    '#description'      => t("The date format in the block.") . ' ' . t("Refer PHP manual for strftime() to learn more about the format."),
    '#default_value'    => variable_get("shootevents_date_short", SHOOTEVENTS_DATE_SHORT),
    '#size'             => 25,
    '#maxlength'        => 255,
    '#required'         => true
  );


  $form['shootevents_list'] = array(
    '#type'             => 'fieldset',
    '#title'            => t('List Settings'),
    '#collapsible'      => true,
    '#collapsed'        => true
  );
  $form['shootevents_list']['shootevents_maxdisp_list']      = array(
    '#type'             => 'textfield',
    '#title'            => t("Maximum number of events on the list"),
    '#description'      => t("The maximum number of links to display on the list."),
    '#default_value'    => variable_get("shootevents_maxdisp_list", SHOOTEVENTS_MAXDISP_LIST),
    '#size'             => 3,
    '#maxlength'        => 3,
    '#required'         => true
  );
  $form['shootevents_list']['shootevents_date_long'] = array(
    '#type'             => 'textfield',
    '#title'            => t("Date format"),
    '#description'      => t("The date format in the list.") . ' ' . t("Refer PHP manual for strftime() to learn more about the format."),
    '#default_value'    => variable_get("shootevents_date_long", SHOOTEVENTS_DATE_LONG),
    '#size'             => 25,
    '#maxlength'        => 255,
    '#required'         => true
  );
  $form['shootevents_list']['shootevents_show_supporter'] = array(
    '#type'             => 'checkbox',
    '#title'            => t("Show supporter"),
    '#description'      => t("If checked, the username of the user who has inserted the event is shown."),
    '#default_value'    => variable_get("shootevents_show_supporter", 1)
  );

  return $form;
}

/**
 * Display Function For The List
 * @version 1
 */
function _shootevents_all($offset = 0) {

  // only administrators can access this module
  if (!user_access('access shootevents')) {

    drupal_set_message(message_access(), 'error');
    drupal_goto('node');
  }

  $content = '';
  $content .= "<div class=\"shootevents\">\n";

  $lim = variable_get("shootevents_maxdisp_list", SHOOTEVENTS_MAXDISP_LIST);
  $sql = sprintf("SELECT e.*,
                         u.name username
                  FROM {shootevents} e,
                       {users} u
                  WHERE e.start>='%s'
                        AND e.uid=u.uid
                  ORDER BY e.start ASC",
                 strftime('%Y-%m-%d', time()));

  $dbr = db_query_range(db_rewrite_sql($sql), (int) $offset, $lim);
  if (db_num_rows($dbr) == 0)
    $content .= "<p>" . t("At this time we do not have any event in the database.") . "</p>\n";
  else {
    global $user;

    // Build output
    $fmt = variable_get("shootevents_date_long", SHOOTEVENTS_DATE_LONG);
    $show_supporter = variable_get("shootevents_show_supporter", true);
    $_dark = false;
    $path   = drupal_get_path('module', 'shootevents');

    $content .= "<table class=\"shootevents\">\n"
        . "<tr>\n"
        . "<th>" . t("Date") . "</th>\n"
        . "<th>" . t("Event") . "</th>\n"
        . "<th>" . t("Federation") . "</th>\n"
        . "</tr>\n";


    while ($row = db_fetch_object($dbr)) {

      $dark = ($_dark ? 'dark' : 'light');
      $_dark = !$_dark;

      $edit_img       = "<img src=\"/$path/edit.gif\" alt=\"" . t("edit") . "\" title=\"".t("Edit this event")."\" />";
      $edit_x_img     = "<img src=\"/$path/edit_x.gif\" alt=\"" . t("edit impossible") . "\" title=\"".t("You cannot edit this event")."\" />";
      $delete_img     = "<img src=\"/$path/delete.gif\" alt=\"" . t("delete") . "\" title=\"".t("Delete this event")."\" />";
      $delete_x_img   = "<img src=\"/$path/delete_x.gif\" alt=\"" . t("delete impossible") . "\" title=\"".t("You cannot delete this event")."\" />";

      $edit           = "<a href=\"" . url(sprintf('shootevents/modify/%u', $row->id)) . "\">$edit_img</a>";
      $edit_x         = $edit_x_img;
      $delete         = "<a href=\"" . url(sprintf('shootevents/delete/%u', $row->id)) . "\">$delete_img</a>";
      $delete_x       = $delete_x_img;

      // Build location string
      //
      if (empty($row->location) && !empty($row->country))
        $location = check_plain($row->country) . '<br />';
      elseif (!empty($row->location) && empty($row->country))
        $location = check_plain($row->location) . '<br />';
      elseif (empty($row->location) && empty($row->country))
        $location = '';
      else
        $location = check_plain($row->location) . ' (' . check_plain($row->country) . ')<br />';

      // build federeation link
      if (empty($row->org_url))
        $org = check_plain($row->org);
      else
        $org = "<a href=\"" . $row->org_url . "\" target=\"_blank\" title=\""
          . sprintf(t("Visit homepage of %s"), check_plain($row->org))
          . "\">" . check_plain($row->org) . "</a>";

      $content .= "<tr class=\"$dark\">\n"
        . "<td class=\"dark\" nowrap>"
        . "<strong>" . strftime($fmt, _shootevents_convert_date($row->start, true));
      if (strcmp('0000-00-00', $row->end))
        $content .= "<br />&nbsp;-&nbsp;"
          . strftime($fmt, _shootevents_convert_date($row->end, true));
      $content .= "</strong>"
        . "</td>\n"
        . "<td class=\"dark\">";

      if ((user_access('create shootevents') && ($user->uid == $row->uid))
              || user_access('administer shootevents'))
        $content .= $delete . $edit;
      elseif (user_access('create shootevents'))
        $content .= $delete_x . $edit_x;

      $content .= "<strong>" . check_plain($row->name) . "</strong><br />\n<small>"
        . sprintf(t("%s, %s targets"), check_plain($row->dis), check_plain($row->targets)) ."<br />\n"
        . $location
        . ($show_supporter ? t("Reported by ") . check_plain($row->username) : '')
        . "</small></td>\n"
        . "<td class=\"dark\">$org"
        . "</td>\n"
        . "</tr>\n";
    }
    $content .= "</table>\n";

    // Build paginator
    //
    $sql = sprintf("SELECT COUNT(*) rows FROM {shootevents} WHERE start>='%s'",
                   strftime('%Y-%m-%d', time()));
    $row = db_fetch_array(db_query_range(db_rewrite_sql($sql), 0, 1));
    $rows = $row['rows'];
    $offsets = array();
    for ($i = 0; $i < $rows; $i += $lim)
        $offsets[] = $i;
    if (count($offsets) > 1)
    {
        $content .= '<div class=\"shootevents-pager\"><p>' . t("Pages") . ': ';
        $first = true;
        foreach ($offsets as $page => $new_offset)
        {
            if (!$first)
                $content .= ', ';
            if ($new_offset == $offset)
                $content .= "<em>" . sprintf('%u', $page+1) . "</em>";
            elseif ($new_offset == 0)
                $content .= l(sprintf('%u', $page+1),
                              sprintf('shootevents', $new_offset));
            else
                $content .= l(sprintf('%u', $page+1),
                              sprintf('shootevents/view/%u', $new_offset));
            $first = false;
        }
    }
    $content .= "</p></div>\n";
  }

  if (user_access('create shootevents')) {

    $content .= "<h2>" . t("Information") . "</h2>"
      . '<p>'
      . t("You are allowed to create new events.") . ' ';

    if (user_access('administer shootevents'))
      $content .= t("You are able to modify and delete all events in the database.");
    else
      $content .= t("You are able to modify and delete the events you have submitted later.");
    $content .= '</p>';
    $content .= '<p>' . t("On deletion <strong>no</strong> further confirmation will be requested.") . '</p>';
  }
  $content .= "</div>\n";
  print theme("page", $content);
}

/**
 * Display Function For The List
 *
 * @version 2
 */
function _shootevents_modify($id)
{
  // can access this module
  if (!user_access('create shootevents')) {
    drupal_set_message(message_access(), 'error');
    drupal_goto('shootevents');
  }

  global $user;

  // Get the fields and save if possible
  if (!strcmp('edit', $_POST['edit']['se_op'])) {
    $se_id = (int) $_POST['edit']['se_id'];
    $se_uid = (int) $_POST['edit']['se_uid'];
    extract(_shootevents_get_validated_fields());
    if (count(form_get_errors()) == 0) {
      // No errors, so store the entry
      //
      if (!user_access('administer shootevents') && !(($user->uid == $_POST['edit']['se_uid']) && user_access('create shootevents')))
      {
          drupal_set_message(t('Access denied!'), 'error');
          drupal_goto('shootevents');
      }
      _shootevents_update($se_id, $se_uid, $se_name, $se_start, $se_end, $se_dis, $se_targets, $se_location, $se_country, $se_org, $se_org_url);
    }
  }
  else
  {
    // First time, get data
    //
    $sql = sprintf("SELECT * FROM {shootevents} WHERE id='%u'", $id);
    $dbr = db_query(db_rewrite_sql($sql));
    if (db_num_rows($dbr) == 0) {
      drupal_set_message(t("Invalid id!"));
      drupal_goto('shootevents');
    }
    $row = db_fetch_array($dbr);
    if (!user_access('administer shootevents') && !(($user->uid == $row['uid']) && user_access('create shootevents'))) {
      drupal_set_message(t('Access denied'), 'error');
      drupal_goto('shootevents');
    }
    // Init vars
    foreach ($row as $field => $value) {
      $var = "se_$field";
      $$var = $value;
    }
    // Beautify some vars
    //
    $se_start   = strftime('%d.%m.%Y', _shootevents_convert_date($se_start, true));
    $se_end     = (strcmp('0000-00-00', $se_end)
                    ? strftime('%d.%m.%Y', _shootevents_convert_date($se_end, true))
                    : '');
  }

  // Create form
  $err = form_get_errors();

  $form = array();
  $form['se_name'] = array(
    '#type'           => 'textfield',
    '#title'          => t("Shooting event name"),
    '#description'    => t("The name of the shooting event, e.g. <strong>F.I.T.A.S.C. World Championship</strong>."),
    '#size'           => 60,
    '#maxlength'      => 255,
    '#required'       => true,
    '#default_value'  => $se_name
  );
  $form['se_start'] = array(
    '#type'           => 'textfield',
    '#title'          => t("First day of the event"),
    '#description'    => t("The first day of the shooting event."),
    '#size'           => 10,
    '#maxlength'      => 10,
    '#required'       => true,
    '#default_value'  => $se_start
  );
  $form['se_end'] = array(
    '#type'           => 'textfield',
    '#title'          => t("Last day of the event"),
    '#description'    => t("The last day of the shooting event if it durate more than one day. Leave blank otherwise."),
    '#size'           => 10,
    '#maxlength'      => 10,
    '#required'       => false,
    '#default_value'  => $se_end
  );
  $form['se_dis'] = array(
    '#type'           => 'select',
    '#title'          => t("Discipline"),
    '#description'    => t("Select the discipline of the shooting event."),
    '#options'        => _shootevents_get_discipline_list(),
    '#required'       => true,
    '#default_value'  => $se_dis
  );
  $form['se_targets'] = array(
    '#type'           => 'select',
    '#title'          => t("Targets"),
    '#description'    => t("Select number of targets of the shooting event."),
    '#options'        => _shootevents_get_target_list(),
    '#required'       => true,
    '#default_value'  => $se_targets
  );
  $form['se_location'] = array(
    '#type'           => 'textfield',
    '#title'          => t("Location"),
    '#description'    => t("The location of the shooting event, e.g. <strong>Jagdparcours Springe</strong>."),
    '#size'           => 60,
    '#maxlength'      => 255,
    '#required'       => false,
    '#default_value'  => $se_location
  );
  $form['se_country'] = array(
    '#type'           => 'textfield',
    '#title'          => t("Country"),
    '#description'    => t("The country of the location of the shooting event, e.g. <strong>Germany</strong>."),
    '#size'           => 60,
    '#maxlength'      => 255,
    '#required'       => false,
    '#default_value'  => $se_country
  );
  $form['se_org'] = array(
    '#type'           => 'textfield',
    '#title'          => t("Federation"),
    '#description'    => t("The organisating federation of the shooting event, e.g. <strong>F.I.T.A.S.C.</strong>."),
    '#size'           => 60,
    '#maxlength'      => 255,
    '#required'       => false,
    '#default_value'  => $se_org
  );
  $form['se_org_url'] = array(
    '#type'           => 'textfield',
    '#title'          => t("Federations website"),
    '#description'    => t("The homepage of the organisating federation, e.g. <strong>http://www.fitasc.com</strong>."),
    '#size'           => 60,
    '#maxlength'      => 255,
    '#required'       => false,
    '#default_value'  => $se_org_url
  );

  $form['se_op']      = array(
    '#type'           => 'hidden',
    '#value'          => 'edit'
  );
  $form['se_id']      = array(
    '#type'           => 'hidden',
    '#value'          => $se_id
  );
  $form['se_uid']      = array(
    '#type'           => 'hidden',
    '#value'          => $se_uid
  );
  $form['submit']     = array(
    '#type'           => 'submit',
    '#value'          => t("Modify")
  );

  $form_content = drupal_get_form('seeditform', $form);

  $content = t("Please make your modifications in the form.");
  $content .= $form_content;

  print theme("page", $content);
}

/**
 * Display Function For The List
 * @param int $id The Id
 */
function _shootevents_delete($id)
{
    // can access this module
    if (!user_access('create shootevents'))
    {
        drupal_set_message(message_access(), 'error');
        drupal_goto('shootevents');
    }

    global $user;

    $sql = sprintf("SELECT uid FROM {shootevents} WHERE id='%u'", $id);
    $dbr = db_query(db_rewrite_sql($sql));
    if (db_num_rows($dbr) == 0)
    {
        drupal_set_message(t("Invalid id!"));
        drupal_goto('shootevents');
    }
    $row = db_fetch_object($dbr);
    if (!user_access('administer shootevents') || !(($user->uid == $row->uid) && user_access('create shootevents')))
    {
        drupal_set_message(message_access(), 'error');
        drupal_goto('shootevents');
    }

    $sql = sprintf("DELETE FROM {shootevents} WHERE id='%u'", $id);
    if (db_query(db_rewrite_sql($sql)))
        drupal_set_message(t("Event removed from database."));
    else
        drupal_set_message(t("An error occured while removing the event."), 'error');
    drupal_goto('shootevents');
}

/**
 * Display Function For The List
 */
function _shootevents_add() {

  // can access this module
  if (!user_access('create shootevents')) {
    drupal_set_message(message_access(), 'error');
    drupal_goto('shootevents');
  }

  // Get the fields and save if possible
  if (!strcmp('add', $_POST['edit']['se_op'])) {
    extract(_shootevents_get_validated_fields());
    if (count(form_get_errors()) == 0) {
      // No errors, so store the entry
      _shootevents_insert($se_name, $se_start, $se_end, $se_dis, $se_targets, $se_location, $se_country, $se_org, $se_org_url);
    }
  }

  // Create form
  $err = form_get_errors();

  $form = array();
  $form['se_name'] = array(
    '#type'           => 'textfield',
    '#title'          => t("Shooting event name"),
    '#description'    => t("The name of the shooting event, e.g. <strong>F.I.T.A.S.C. World Championship</strong>."),
    '#size'           => 60,
    '#maxlength'      => 255,
    '#required'       => true,
    '#default_value'  => $se_name
  );
  $form['se_start'] = array(
    '#type'           => 'textfield',
    '#title'          => t("First day of the event"),
    '#description'    => t("The first day of the shooting event."),
    '#size'           => 10,
    '#maxlength'      => 10,
    '#required'       => true,
    '#default_value'  => $se_start
  );
  $form['se_end'] = array(
    '#type'           => 'textfield',
    '#title'          => t("Last day of the event"),
    '#description'    => t("The last day of the shooting event if it durate more than one day. Leave blank otherwise."),
    '#size'           => 10,
    '#maxlength'      => 10,
    '#required'       => false,
    '#default_value'  => $se_end
  );
  $form['se_dis'] = array(
    '#type'           => 'select',
    '#title'          => t("Discipline"),
    '#description'    => t("Select the discipline of the shooting event."),
    '#options'        => _shootevents_get_discipline_list(),
    '#required'       => true,
    '#default_value'  => $se_dis
  );
  $form['se_targets'] = array(
    '#type'           => 'select',
    '#title'          => t("Targets"),
    '#description'    => t("Select number of targets of the shooting event."),
    '#options'        => _shootevents_get_target_list(),
    '#required'       => true,
    '#default_value'  => $se_targets
  );
  $form['se_location'] = array(
    '#type'           => 'textfield',
    '#title'          => t("Location"),
    '#description'    => t("The location of the shooting event, e.g. <strong>Jagdparcours Springe</strong>."),
    '#size'           => 60,
    '#maxlength'      => 255,
    '#required'       => false,
    '#default_value'  => $se_location
  );
  $form['se_country'] = array(
    '#type'           => 'textfield',
    '#title'          => t("Country"),
    '#description'    => t("The country of the location of the shooting event, e.g. <strong>Germany</strong>."),
    '#size'           => 60,
    '#maxlength'      => 255,
    '#required'       => false,
    '#default_value'  => $se_country
  );
  $form['se_org'] = array(
    '#type'           => 'textfield',
    '#title'          => t("Federation"),
    '#description'    => t("The organisating federation of the shooting event, e.g. <strong>F.I.T.A.S.C.</strong>."),
    '#size'           => 60,
    '#maxlength'      => 255,
    '#required'       => false,
    '#default_value'  => $se_org
  );
  $form['se_org_url'] = array(
    '#type'           => 'textfield',
    '#title'          => t("Federations website"),
    '#description'    => t("The homepage of the organisating federation, e.g. <strong>http://www.fitasc.com</strong>."),
    '#size'           => 60,
    '#maxlength'      => 255,
    '#required'       => false,
    '#default_value'  => $se_org_url
  );

  $form['se_op']      = array(
    '#type'           => 'hidden',
    '#value'          => 'add'
  );
  $form['submit']     = array(
    '#type'           => 'submit',
    '#value'          => t("Add")
  );

  $form_content = drupal_get_form('seaddform', $form);

  $content = t("To add a shooting event fill out the form.")
    . $form_content;

  print theme("page", $content);
}

/**
 * Validate A Date
 * @param string $date The date in the format DD.MM.YYYY
 * @return int The timestamp or FALSE on error
 */
function _shootevents_convert_date($date, $from_db = false)
{
    if ($from_db)
        list ($year,
              $month,
              $day) = explode('-', $date);
    else
    {
        // Split
        if (!preg_match("/([0-9]{1,2})\.([0-9]{1,2})\.([0-9]{4})/", $date, $r))
            return false;
        list ($dummy, $day, $month, $year) = $r;
    }

    // Validate date itsself
    if (!checkdate($month, $day, $year))
        return false;

    $result = mktime(0, 0, 0, $month, $day, $year);
    return $result;
}

/**
 * Return Timestamp Of the Current Day Without Time
 * @return int
 */
function _shootevents_date()
{
    static $result;

    if (isset($result))
        return $result;

    $result = mktime(0, 0, 0, date('n'), date('j'), date('Y'));
    return $result;
}

function _shootevents_get_validated_fields()
{
    // Validate data at all
    //
//    if (!valid_input_data($_POST['edit']))
//    {
//        form_set_error('se_op', t("Your input ist not acceptable."));
//        return array();
//    }

    // Required name
    $se_name        = trim($_POST['edit']['se_name']);
    if (empty($se_name))
        form_set_error('se_name', t("You must enter a name."));

    // Validate date
    $se_start       = trim($_POST['edit']['se_start']);
    $se_end         = trim($_POST['edit']['se_end']);
    if (empty($se_start))
        form_set_error('se_start', t("You must enter the first day."));
    elseif (false === ($_se_start = _shootevents_convert_date($se_start)))
        form_set_error('se_start', t("The given date of the first day is invalid."));
    elseif (_shootevents_date() > $_se_start)
        form_set_error('se_start', t("You cannot set up an event in the past."));
    elseif (!empty($se_end))
    {
        if (false === ($_se_end = _shootevents_convert_date($se_end)))
            form_set_error('se_end', t("The date of the last day is invalid."));
        elseif ($_se_end < $_se_start)
            form_set_error('se_end', t("The last day must be later than the first day"));
    }
    $se_location    = trim($_POST['edit']['se_location']);
    $se_country     = trim($_POST['edit']['se_country']);
    $se_dis         = trim($_POST['edit']['se_dis']);
    $se_targets     = trim($_POST['edit']['se_targets']);
    $se_org         = trim($_POST['edit']['se_org']);
    $se_org_url     = trim($_POST['edit']['se_org_url']);
    if (!empty($se_org_url) && empty($se_org))
        form_set_error('se_org', t("If an URL is given for the federation, you must enter the name of the federation as well."));
    if (!empty($se_org_url) && !valid_url($se_org_url, true))
        form_set_error('se_org_url', t("The format of the URL of the federation is invalid."));
    return get_defined_vars();
}

/**
 * Create An Array With Field Names And Values
 * @return array
 */
function _shootevents_make_field_array($se_uid, $se_name, $se_start, $se_end, $se_dis, $se_targets, $se_location, $se_country, $se_org, $se_org_url)
{
    $fields = array('name'      => db_escape_string($se_name),
                    'uid'       => db_escape_string($se_uid),
                    'start'     => strftime('%Y-%m-%d', _shootevents_convert_date($se_start)),
                    'dis'       => db_escape_string($se_dis),
                    'targets'   => db_escape_string($se_targets));
    if (!empty($se_end))
        $fields['end'] = strftime('%Y-%m-%d', _shootevents_convert_date($se_end));
    if (!empty($se_location))
        $fields['location'] = db_escape_string($se_location);
    if (!empty($se_country))
        $fields['country'] = db_escape_string($se_country);
    if (!empty($se_org))
        $fields['org'] = db_escape_string($se_org);
    if (!empty($se_org_url))
        $fields['org_url'] = db_escape_string($se_org_url);
    return $fields;
}

/**
 * Insert A New Record
 */
function _shootevents_insert($se_name, $se_start, $se_end, $se_dis, $se_targets, $se_location, $se_country, $se_org, $se_org_url)
{
    global $user;

    $fields = _shootevents_make_field_array($user->uid,
                                            $se_name,
                                            $se_start,
                                            $se_end,
                                            $se_dis,
                                            $se_targets,
                                            $se_location,
                                            $se_country,
                                            $se_org,
                                            $se_org_url);
    $sql = "INSERT INTO {shootevents} (" . join(', ', array_keys($fields)) . ") VALUES ('" . join("', '", $fields) . "')";

    if (db_query(db_prefix_tables($sql)))
        drupal_set_message(sprintf(t("The event '%s' is saved."), check_plain($se_name)));
    else
        drupal_set_message(sprintf(t("An error occured while saving the event '%s'."), check_plain($se_name)), 'error');
    drupal_goto('shootevents/add');
}

/**
 * Update A Record
 */
function _shootevents_update($se_id, $se_uid, $se_name, $se_start, $se_end, $se_dis, $se_targets, $se_location, $se_country, $se_org, $se_org_url)
{
    $fields = _shootevents_make_field_array($se_uid,
                                            $se_name,
                                            $se_start,
                                            $se_end,
                                            $se_dis,
                                            $se_targets,
                                            $se_location,
                                            $se_country,
                                            $se_org,
                                            $se_org_url);

    $sets = array();
    foreach ($fields as $field => $value)
        $sets[] = "$field='$value'";
    $sql = sprintf("UPDATE {shootevents} SET " . join(', ', $sets) . "WHERE id='%u'", $se_id);

    if (db_query(db_prefix_tables($sql)))
        drupal_set_message(sprintf(t("The event '%s' is saved."), check_plain($se_name)));
    else
        drupal_set_message(sprintf(t("An error occured while saving the event '%s'."), check_plain($se_name)), 'error');
    drupal_goto('shootevents');
}

/**
 * Return The Formular
 * @return string The HTML
 */
function _shootevents_get_form($se_name, $se_start, $se_end, $se_dis, $se_targets, $se_location, $se_country, $se_org, $se_org_url)
{
  $err = form_get_errors();


//
//    $form_content = '';
//
//    return $form_content;

}

/**
 * Return A List Of Available Disciplines
 * @return array
 */
function _shootevents_get_discipline_list()
{
    static $result;

    if (isset($result))
        return $result;

    // Select available disciplines
    $_dis = explode(',', variable_get("shootevents_dis", SHOOTEVENTS_DIS_DEFAULT));
    $result = array();
    foreach ($_dis as $dis)
        $result[$dis]= $dis;
    return $result;
}

/**
 * Return A List Of Available Target Numbers
 * @return array
 */
function _shootevents_get_target_list()
{
    static $result;

    if (isset($result))
        return $result;

    // Select available disciplines
    $_dis = explode(',', variable_get("shootevents_targets", SHOOTEVENTS_TARGETS_DEFAULT));
    $result = array();
    foreach ($_dis as $dis)
        $result[$dis]= $dis;
    return $result;
}

/*
 * $Log$
 * Revision 1.4  2006/09/06 18:40:55  hinrich
 * - Absolute path for images
 *
 * Revision 1.3  2006/03/18 13:44:47  hinrich
 * - Fix missing arg
 *
 * Revision 1.2  2006/02/27 12:18:41  hinrich
 * - adjust on 4.7
 *
 * Revision 1.1  2005/11/29 16:38:08  hinrich
 * - Init
 *
 */

?>
